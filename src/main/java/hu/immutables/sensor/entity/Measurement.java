package hu.immutables.sensor.entity;

//Keszitette:Raczkovi Zsolt

import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Measurement")
public class Measurement extends BusinessObject{

	private long measurementId;
	private Sensor sensor;
	private double value;
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime measurementTimestamp;
	public long getMeasurementId() {
		return measurementId;
	}
	public void setMeasurementId(long measurementId) {
		this.measurementId = measurementId;
	}
	public Sensor getSensor() {
		return sensor;
	}
	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public LocalDateTime getMeasurementTimestamp() {
		return measurementTimestamp;
	}
	public void setMeasurementTimestamp(LocalDateTime measurementTimestamp) {
		this.measurementTimestamp = measurementTimestamp;
	}
	
	
}
