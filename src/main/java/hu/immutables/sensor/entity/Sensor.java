package hu.immutables.sensor.entity;
//Keszitette:Raczkovi Zsolt

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Sensor")
public class Sensor extends BusinessObject{

	private String sensorId;
	private String sensorName;
	private double latitude;
	private double longitude;
	
	public String getSensorId() {
		return sensorId;
	}
	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}
	public String getSensorName() {
		return sensorName;
	}
	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	
	
}
