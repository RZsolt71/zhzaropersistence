package hu.immutables.sensor.dto;

//Keszitette:Raczkovi Zsolt

import java.time.LocalDateTime;

public class MeasurementDTO extends BusinessObjectDTO{

	private long measurementId;
	private SensorDTO sensor;
	private double value;
	private LocalDateTime measurementTimestamp;
	public long getMeasurementId() {
		return measurementId;
	}
	public void setMeasurementId(long measurementId) {
		this.measurementId = measurementId;
	}
	public SensorDTO getSensor() {
		return sensor;
	}
	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public LocalDateTime getMeasurementTimestamp() {
		return measurementTimestamp;
	}
	public void setMeasurementTimestamp(LocalDateTime measurementTimestamp) {
		this.measurementTimestamp = measurementTimestamp;
	}
	
	
}
