package hu.immutables.sensor.mapper;

//Keszitette:Raczkovi Zsolt

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

public class EntityMapper<E, DTO> implements Mapper<E, DTO> {

	protected ModelMapper mapper = new ModelMapper();

	protected Class<E> entityClazz;
	protected Class<DTO> dtoCLazz;

	public EntityMapper(Class<E> entityClazz, Class<DTO> dtoCLazz) {
		this.entityClazz = entityClazz;
		this.dtoCLazz = dtoCLazz;
	}
	
	@Override
	public E transformDTO(DTO object) {

		return mapper.map(object, entityClazz);
	}

	@Override
	public DTO transformEntity(E entity) {

		return mapper.map(entity, dtoCLazz);
	}

	@Override
	public List<E> transformDTOs(List<DTO> dtos) {

		List<E> entities = new ArrayList<E>();

		for (DTO dto : dtos) {
			E currentEntity = transformDTO(dto);
			entities.add(currentEntity);
		}

		return entities;
	}

	@Override
	public List<DTO> transformEntities(List<E> entities) {

		List<DTO> dtoList = new ArrayList<DTO>();

		for (E entity : entities) {
			DTO currentDTO = transformEntity(entity);
			dtoList.add(currentDTO);
		}
		return dtoList;
	}

}
