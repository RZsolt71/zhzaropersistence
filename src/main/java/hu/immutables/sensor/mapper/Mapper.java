package hu.immutables.sensor.mapper;
//Keszitette:Raczkovi Zsolt


import java.util.List;

public interface Mapper <E, DTO>{

	E transformDTO(DTO object);
	DTO transformEntity(E entity);
	List<E> transformDTOs(List<DTO> dtos);
	List<DTO> transformEntities(List<E> entities);
}
