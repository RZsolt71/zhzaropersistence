package hu.immutables.sensor.jpadao;
import hu.immutables.sensor.dao.SensorDAO;
import hu.immutables.sensor.dto.SensorDTO;
import hu.immutables.sensor.entity.Sensor;

public class DefaultSensorDAO extends JpaDefaultBaseDAO<Sensor, SensorDTO> implements SensorDAO{

	@Override
	public SensorDTO getBySensorId(String id) {
		return (SensorDTO) em.createQuery("SELECT r FROM Sensor r WHERE r.sensorId = :id")
				.setParameter("id", id).getSingleResult();
	}

	@Override
	public SensorDTO getBySensorName(String sensorName) {
		return (SensorDTO) em.createQuery("SELECT r FROM Sensor r WHERE r.sensorName = :sensorName")
				.setParameter("sensorName", sensorName).getSingleResult();
	}

}
