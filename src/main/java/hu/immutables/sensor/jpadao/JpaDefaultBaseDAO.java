package hu.immutables.sensor.jpadao;
//Keszitette:Raczkovi Zsolt
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.core.GenericTypeResolver;

import hu.immutables.sensor.dao.BaseDAO;
import hu.immutables.sensor.dto.BusinessObjectDTO;
import hu.immutables.sensor.entity.BusinessObject;
import hu.immutables.sensor.mapper.EntityMapper;


public abstract class JpaDefaultBaseDAO<E extends BusinessObject, DTO extends BusinessObjectDTO>
		implements BaseDAO<DTO> {

	@PersistenceContext(name = "essPU")
	protected EntityManager em;
	protected Class<E> entityClazz;
	protected EntityMapper<E, DTO> entityMapper;

	public void setupEntityManager(EntityManager em) {
		this.em = em;
	}

	@SuppressWarnings("unchecked")
	public JpaDefaultBaseDAO() {
		
		Class<?>[] clazzes = GenericTypeResolver.resolveTypeArguments(getClass(), JpaDefaultBaseDAO.class);

		Class<DTO> dtoClazz = null;

		if (clazzes != null && clazzes.length == 2) {

			for (Class<?> c : clazzes) {

				if (BusinessObject.class.isAssignableFrom(c)) {
					entityClazz = (Class<E>) c;
				} 
				else if (BusinessObjectDTO.class.isAssignableFrom(c)) {
					dtoClazz = (Class<DTO>) c;
				}
			}

		} else {
			throw new IllegalArgumentException("Couldn't determine business object or dto types!");
		}

		entityMapper = new EntityMapper<E, DTO>(entityClazz, dtoClazz);
	}

	public Long merge(DTO object) {
		E entity = entityMapper.transformDTO(object);
		if (!em.contains(entity)) {
			entity=em.merge(entity);
			em.flush();
		}
		return entity.getId();
	}

	public void delete(DTO object) {
		E entity = entityMapper.transformDTO(object);
		em.remove(em.merge(entity));
	}

	@SuppressWarnings("unchecked")
	public List<DTO> getAll() {
		return entityMapper
				.transformEntities(em.createQuery("Select e from " + entityClazz.getSimpleName() + " e").getResultList());
	}

	public DTO getById(Long id) {
		E entity = em.find(entityClazz, id);
		return entityMapper.transformEntity(entity);
	}

	public void update(DTO object) {
		merge(object);

	}

	public Long insert(DTO object) {
		return merge(object);
	}
}
