package hu.immutables.sensor.jpadao;
import hu.immutables.sensor.dao.MeasurementDAO;
import hu.immutables.sensor.dto.MeasurementDTO;
import hu.immutables.sensor.entity.Measurement;

public class DefaultMeasuremetDAO extends JpaDefaultBaseDAO<Measurement, MeasurementDTO> implements MeasurementDAO{

	@Override
	public MeasurementDTO getByMeasurementId(Long id) {
			return (MeasurementDTO) em.createQuery("SELECT r FROM Measurement r WHERE r.measurementId = :id")
							.setParameter("id", id).getSingleResult();
		}

}
