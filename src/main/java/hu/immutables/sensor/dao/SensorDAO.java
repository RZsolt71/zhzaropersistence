package hu.immutables.sensor.dao;

import hu.immutables.sensor.dto.SensorDTO;

public interface SensorDAO extends BaseDAO<SensorDTO>{

	public SensorDTO getBySensorId(String id);
	public SensorDTO getBySensorName(String sensorName);
	

}
