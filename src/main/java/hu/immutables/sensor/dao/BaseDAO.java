package hu.immutables.sensor.dao;
//Keszitette:Raczkovi Zsolt

import java.util.List;

import hu.immutables.sensor.dto.BusinessObjectDTO;

public interface BaseDAO<DTO extends BusinessObjectDTO> {
	
	void update(DTO object);
	
	Long insert(DTO object);
	
	void delete(DTO object);
	
	List<DTO> getAll();
	
	DTO getById(Long id);
}
