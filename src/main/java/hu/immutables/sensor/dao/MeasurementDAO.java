package hu.immutables.sensor.dao;
//Keszitette:Raczkovi Zsolt

import hu.immutables.sensor.dto.MeasurementDTO;

public interface MeasurementDAO extends BaseDAO<MeasurementDTO> {

	public MeasurementDTO getByMeasurementId(Long id);
	
}
