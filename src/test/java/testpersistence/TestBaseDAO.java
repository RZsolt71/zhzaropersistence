package testpersistence;
//Keszitette:Raczkovi Zsolt

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import hu.immutables.sensor.dao.MeasurementDAO;
import hu.immutables.sensor.dao.SensorDAO;
import hu.immutables.sensor.dto.MeasurementDTO;
import hu.immutables.sensor.dto.SensorDTO;
import hu.immutables.sensor.jpadao.DefaultMeasuremetDAO;
import hu.immutables.sensor.jpadao.DefaultSensorDAO;

public class TestBaseDAO {

	private EntityManager em;
	private static EntityManagerFactory factory;
	private static final String PU_NAME = "essTestPU";


	@BeforeClass
	public static void initFactory() {
		factory = Persistence.createEntityManagerFactory(PU_NAME);
	}

	@Before
	public void init() {
		em = factory.createEntityManager();

	}

	@Test
	public void test_datainsert() {

		try {
			SensorDAO sensorDAO = new DefaultSensorDAO();
			((DefaultSensorDAO) sensorDAO).setupEntityManager(em);
			MeasurementDAO measurementDAO = new DefaultMeasuremetDAO();
			((DefaultMeasuremetDAO) measurementDAO).setupEntityManager(em);

			em.getTransaction().begin();

			SensorDTO sensor1 = new SensorDTO();
			sensor1.setSensorId("sensor1");
			sensor1.setLatitude(1000);
			sensor1.setLongitude(-100);
			sensor1.setSensorName("name1");

			sensorDAO.insert(sensor1);

			SensorDTO sensor2 = new SensorDTO();
			sensor2.setSensorId("sensor2");
			sensor2.setLatitude(2000);
			sensor2.setLongitude(-200);
			sensor2.setSensorName("name2");

			sensorDAO.insert(sensor2);

			MeasurementDTO measurement1 = new MeasurementDTO();
			measurement1.setMeasurementId(1L);
			measurement1.setMeasurementTimestamp(LocalDateTime.of(2018, 5, 11, 20, 45, 55));
			measurement1.setSensor(sensor1);
			measurement1.setValue(20);

			measurementDAO.insert(measurement1);

			em.getTransaction().commit();

			assertEquals(2, sensorDAO.getAll().size());
			assertEquals(1, measurementDAO.getAll().size());
			assertEquals("name1", sensorDAO.getBySensorName("name1").getSensorName());
			

			em.getTransaction().begin();

			SensorDTO sensor3=sensorDAO.getBySensorName("name2");
			sensor3.setSensorName("name10");
			assertEquals("name10", sensorDAO.getBySensorName("name10").getSensorName());
			
			em.getTransaction().commit();

		} catch (Exception ex) {

			if (em != null && em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}

			//ex.printStackTrace();
		}

	}
}